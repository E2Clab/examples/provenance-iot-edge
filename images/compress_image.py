import zlib
import logging

logging.basicConfig(level=logging.INFO)


def _decompress(file):
    with open(file, "rb") as in_file:
        decompressed = zlib.decompress(in_file.read())
        print(f"decompress={in_file.tell()}")
    with open("decompressed-image.jpg", "wb") as out_file:
        out_file.write(decompressed)
        print(f"decompress={out_file.tell()}")


def _compress(file):
    with open(file, "rb") as in_file:
        compressed = zlib.compress(in_file.read(), -1)
        print(f"compress={in_file.tell()}")
    with open("compressed-image", "wb") as out_file:
        out_file.write(compressed)
        print(f"compress={out_file.tell()}")


if __name__ == '__main__':
    _compress("/home/drosendo/git/provenance-iot-edge/images/output-664.jpg")
    _decompress("/home/drosendo/git/provenance-iot-edge/images/compressed-image")

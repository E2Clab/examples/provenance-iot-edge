from provlake import ProvLake
from provlake.capture import ProvWorkflow, ProvTask
"""
https://github.com/IBM/multi-data-lineage-capture-py
Very simple example to show how ProvLake is used to instrument a simple python script for provenance data management.
One workflow with 1 task.
"""

import logging
import time
logging.basicConfig(level=logging.DEBUG)


def calc_factorial(n):
    num = n
    result = 1
    while num > 1:
        result = result * num
        num = num - 1
    return result


# prov = ProvLake.get_persister("factorial_dataflow", service_url="http://127.0.0.1:22000")
# prov_workflow = ProvWorkflow(prov, workflow_name="aaa")
# prov_workflow.begin()

in_args = {"n": 60000}
# prov_task = ProvTask(prov, "factorial_number", input_args=in_args)
# prov_task.begin()
start = time.time()
factorial = calc_factorial(in_args.get("n"))
end = time.time()

out_args = {"factorial": factorial}
print(f"execution time (sec) = {end - start}")
# prov_task.end(out_args)
#
# prov_workflow.end()

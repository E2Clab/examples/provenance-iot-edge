import logging
import time
logging.basicConfig(level=logging.DEBUG)


def calc_factorial(n):
    num = n
    result = 1
    while num > 1:
        result = result * num
        num = num - 1
    return result


in_args = {"n": 9800}
start = time.time()
for i in range(10):
    factorial = calc_factorial(in_args.get("n"))
end = time.time()
out_args = {"factorial": factorial}
print(f"execution time (sec) = {end - start}")

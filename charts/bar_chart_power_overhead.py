import numpy as np
import matplotlib.pyplot as plt

# Enter raw data
provlake = np.array([6.57])
dfanalyzer = np.array([7.30])
provlight = np.array([2.55])

# Calculate the average
provlake_mean = np.mean(provlake)
dfanalyzer_mean = np.mean(dfanalyzer)
provlight_mean = np.mean(provlight)

# Calculate the standard deviation
provlake_std = np.std(provlake)
dfanalyzer_std = np.std(dfanalyzer)
provlight_std = np.std(provlight)

# Create lists for the plot
materials = ['ProvLake', 'DfAnalyzer', 'ProvLight']
bar_color = ['#cc0000', '#e69138', '#45818e']
x_pos = np.arange(len(materials))
CTEs = [provlake_mean, dfanalyzer_mean, provlight_mean]
error = [provlake_std, dfanalyzer_std, provlight_std]


# Build the plot
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 16
fig, ax = plt.subplots()
ax.bar(x_pos, CTEs, align='center', alpha=0.5, ecolor='black', capsize=10, color=bar_color)
ax.set_ylabel('Overhead (%)', fontsize=MEDIUM_SIZE)
ax.set_xticks(x_pos)
ax.set_xticklabels(materials, fontsize=MEDIUM_SIZE)
ax.set_title('Power Consumption Overhead (%)', fontsize=BIGGER_SIZE)
ax.yaxis.grid(True)
plt.ylim((0, 10))

# plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
# plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
# plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
# plt.rc('ytick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Save the figure and show
plt.tight_layout()
plt.savefig('bar_plot_power_overhead.png')
plt.show()

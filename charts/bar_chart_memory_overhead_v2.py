import numpy as np
import matplotlib.pyplot as plt

# Create lists for the plot
materials = ['ProvLake', 'DfAnalyzer', 'ProvLight']
bar_color = ['#cc0000', '#e69138', '#45818e']
x_pos = np.arange(len(materials))
CTEs = [7.76, 7.5, 3.9]
error = [0.03, 0, 0]


# Build the plot
SMALL_SIZE = 10
MEDIUM_SIZE = 12
BIGGER_SIZE = 18
fig, ax = plt.subplots()
ax.bar(x_pos, CTEs, yerr=error, align='center', alpha=0.5, ecolor='black', capsize=10, color=bar_color)
ax.set_ylabel('Overhead (%)', fontsize=BIGGER_SIZE)
ax.set_xticks(x_pos)
ax.set_xticklabels(materials, fontsize=BIGGER_SIZE)
ax.set_title('Memory Usage Overhead (%)', fontsize=BIGGER_SIZE)
ax.yaxis.grid(True)
ax.set_yticks([0, 2, 4, 6, 8, 10], fontsize=BIGGER_SIZE)
ax.set_yticklabels([0, 2, 4, 6, 8, 10], fontsize=BIGGER_SIZE)
# plt.rc('font', size=SMALL_SIZE)          # controls default text sizes
# plt.rc('axes', titlesize=SMALL_SIZE)     # fontsize of the axes title
# plt.rc('axes', labelsize=MEDIUM_SIZE)    # fontsize of the x and y labels
# plt.rc('xtick', labelsize=SMALL_SIZE)    # fontsize of the tick labels
plt.rc('ytick', labelsize=BIGGER_SIZE)    # fontsize of the tick labels
# plt.rc('legend', fontsize=SMALL_SIZE)    # legend fontsize
# plt.rc('figure', titlesize=BIGGER_SIZE)  # fontsize of the figure title

# Save the figure and show
plt.tight_layout()
plt.savefig('bar_plot_memory_overhead.png')
plt.show()

from e2clab.services import Service
import enoslib as en


class DfaServer(Service):

    def deploy(self):
        with en.actions(roles=self.roles) as a:
            # install MQTT-SN broker
            a.git(repo="https://github.com/eclipse/mosquitto.rsmb.git", dest="~/mosquitto.rsmb/", force=True)
            a.shell("cd /root/mosquitto.rsmb/rsmb/src/ && make")
            # install MQTT-SN client
            a.git(repo="https://github.com/luanguimaraesla/mqttsn.git", dest="~/mqttsn/", force=True)
            a.shell("pip3 install -U -e mqttsn/")
            # install uwsgi, flask
            a.pip(name="uwsgi")
            a.pip(name="flask")

        return self.register_service(service_port=22000)

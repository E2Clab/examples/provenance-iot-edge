from e2clab.services import Service
import enoslib as en

DF_ANALYZER_NAME = "dfanalyzer"
DF_ANALYZER_PORT = 22000


class Provenance(Service):

    def deploy(self):
        # install docker
        registry_opts = dict(type="external", ip="docker-cache.grid5000.fr", port=80)
        self.deploy_docker(registry_opts=registry_opts)

        with en.actions(roles=self.roles) as a:
            # DfAnalyzer container
            a.docker_container(name=DF_ANALYZER_NAME,
                               image="vitorss/dataflow_analyzer",
                               restart="yes",
                               restart_policy="always",
                               published_ports=f"{DF_ANALYZER_PORT}:{DF_ANALYZER_PORT}",
                               command="bash -c "
                                       "'cd dfanalyzer/applications/dfanalyzer/ && "
                                       "./start-dfanalyzer.sh'")
            # install MQTT-SN broker
            a.git(repo="https://github.com/eclipse/mosquitto.rsmb.git", dest="~/mosquitto.rsmb/", force=True)
            a.shell("cd ~/mosquitto.rsmb/rsmb/src/ && make")
            # install MQTT-SN client
            a.git(repo="https://github.com/luanguimaraesla/mqttsn.git", dest="~/mqttsn/", force=True)
            a.shell("pip3 install -U -e mqttsn/")
            # install DfAnalyzer python lib
            a.git(repo="https://gitlab.com/ssvitor/dataflow_analyzer.git", dest="~/dataflow_analyzer/", force=True)
            a.shell("cp -rf dataflow_analyzer/library/dfa-lib-python . && rm -rf dataflow_analyzer/")
            a.shell("pip3 install -e dfa-lib-python/")
            # Enforce the dataflow file provided by users
            a.copy(
                src="~/git/provenance-iot-edge/artifacts/create-dataflow-specification-dfanalyzer.py",
                dest="/tmp/create-dataflow-specification-dfanalyzer.py"
            )
            a.shell("export DFA_URL=http://localhost:22000 && python /tmp/create-dataflow-specification-dfanalyzer.py &")
            # Start MQTT-SN broker
            #  FIXME define file location (should it the in 'e2clab/services/' or artifacts ?)
            a.copy(
                src="~/git/provenance-iot-edge/artifacts/config.conf",
                dest="~/mosquitto.rsmb/rsmb/src/config.conf"
            )
            a.shell(
                chdir="~/mosquitto.rsmb/rsmb/src/",
                cmd="./broker_mqtts config.conf &"
            )
            # Start translator
            #  FIXME add it to 'e2clab/services/'
            a.copy(
                src="~/git/provenance-iot-edge/artifacts/server_mqttsn_translator.py",
                dest="/tmp/server_mqttsn_translator.py"
            )
            a.shell("nohup python3 /tmp/server_mqttsn_translator.py --client_id 1 --log /tmp/translator.log &")

        return self.register_service(service_port=DF_ANALYZER_PORT)

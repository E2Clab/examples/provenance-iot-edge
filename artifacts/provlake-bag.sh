#!/bin/bash
for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 10 --log /tmp/provlake-out-05s-b10.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 10 --log /tmp/provlake-out-1s-b10.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 20 --log /tmp/provlake-out-05s-b20.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 20 --log /tmp/provlake-out-1s-b20.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 50 --log /tmp/provlake-out-05s-b50.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 50 --log /tmp/provlake-out-1s-b50.log
   sleep $2
done

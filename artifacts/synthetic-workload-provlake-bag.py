import argparse
import logging
import time
from provlake import ProvLake
from provlake.capture import ProvWorkflow, ProvTask
import os

provlake_url = os.environ.get('PROVLAKE_URL', "http://localhost:22000/")

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--number_of_transformations",
        type=int,
        required=True,
        help="Number of transformations.",
    )
    parser.add_argument(
        "--number_of_tasks",
        type=int,
        required=True,
        help="Number of tasks.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=True,
        help="Number of data values per task (each data value is about the same size).",
    )
    parser.add_argument(
        "--task_duration",
        type=float,
        required=True,
        help="Duration of the task in secs.",
    )
    parser.add_argument(
        "--bag",
        type=int,
        required=True,
        help="Bag size.",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=True,
        help="Path to log file.",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.INFO)

    # NOTE: Prospective Provenance
    dataflow_tag = "synthetic-workload-prov"
    prov = ProvLake.get_persister(dataflow_tag, service_url=provlake_url, bag_size=args.bag,
                                  log_level="INFO", should_send_to_file=False)
    prov_workflow = ProvWorkflow(prov, workflow_name=dataflow_tag)
    prov_workflow.begin()
    # NOTE: Retrospective Provenance
    input_task_elements = {'in_task': [1 for _ in range(args.data_values_per_task)]}
    output_task_elements = {'out_task': [2 for _ in range(args.data_values_per_task)]}
    start = time.time()

    for _id_tf in range(args.number_of_transformations):
        for _id_tk in range(int(args.number_of_tasks/args.number_of_transformations)):
            prov_task = ProvTask(prov, f"tf{_id_tf}_tk{_id_tk}", input_args=input_task_elements)
            prov_task.begin()
            # time_begin = time.time()
            # while time.time() - time_begin < args.task_duration:
            #     continue
            time.sleep(args.task_duration)
            prov_task.end(output_task_elements)

    end = time.time()

    logging.info(f"Synthetic workload capture [PROVLAKE]: [task_duration={args.task_duration} / "
                 f"number_of_transformations={args.number_of_transformations} / "
                 f"number_of_tasks={args.number_of_tasks} / "
                 f"data_values_per_task={args.data_values_per_task}] "
                 f"execution time (sec) = {end - start}")

from flask import Flask, make_response, request

app = Flask(__name__)


def read_respond():
    print(request.data)
    print("\n")
    return make_response("{'1':'a'}")


@app.route("/")
def liveness():
    return "Server up!"


@app.route('/pde/dataflow/json', methods=['POST'])
def dfanalyzer_dataflow():
    return read_respond()


@app.route('/pde/task/json', methods=['POST'])
def dfanalyzer_task():
    return read_respond()


@app.route('/retrospective-provenance', methods=['POST'])
def provlake():
    return read_respond()

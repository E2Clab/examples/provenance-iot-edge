import argparse
from dfa_lib_python.dataflow import Dataflow
from dfa_lib_python.transformation import Transformation
from dfa_lib_python.attribute import Attribute
from dfa_lib_python.attribute_type import AttributeType
from dfa_lib_python.set import Set
from dfa_lib_python.set_type import SetType


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--number_of_transformations",
        type=int,
        required=False,
        default=5,
        help="Number of transformations.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=False,
        default=2,
        help="Number of data values per task (each data value is about the same size).",
    )
    args = parser.parse_args()

    dataflow_tag = "1"
    df = Dataflow(dataflow_tag)
    # NOTE: Prospective Provenance
    for _id in range(args.number_of_transformations):
        transformation = Transformation(f"tf{_id}")
        transformations_input = Set(f"itf{_id}" if _id < 1 else f"otf{_id-1}", SetType.INPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                        for attribute in range(args.data_values_per_task)])
        if _id > 0:
            transformations_input.dependency = f"tf{_id-1}".lower()
        transformations_output = Set(f"otf{_id}", SetType.OUTPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                     for attribute in range(args.data_values_per_task)])
        transformation.set_sets([transformations_input, transformations_output])
        df.add_transformation(transformation)
    df.save()

    # Transformation model training
    # tf1 = Transformation("model_training")
    # tf1_input = Set("training_input", SetType.INPUT,
    #                 [Attribute("kernel_size", AttributeType.NUMERIC),
    #                  Attribute("num_kernels", AttributeType.NUMERIC),
    #                  Attribute("length_of_strides", AttributeType.NUMERIC),
    #                  Attribute("pooling_size", AttributeType.NUMERIC),
    #                  ])
    # tf1_output = Set("training_output", SetType.OUTPUT,
    #                  [Attribute("accuracy", AttributeType.NUMERIC),
    #                   Attribute("training_time", AttributeType.NUMERIC)])
    # tf1.set_sets([tf1_input, tf1_output])
    # df.add_transformation(tf1)
    # df.save()

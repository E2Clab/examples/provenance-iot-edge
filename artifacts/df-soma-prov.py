import argparse
import logging
import os
import time
from dfa_lib_python.dataflow import Dataflow
from dfa_lib_python.transformation import Transformation
from dfa_lib_python.attribute import Attribute
from dfa_lib_python.attribute_type import AttributeType
from dfa_lib_python.set import Set
from dfa_lib_python.set_type import SetType
from dfa_lib_python.task import Task
from dfa_lib_python.dataset import DataSet
from dfa_lib_python.element import Element


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Dfa")
    parser.add_argument(
        "--input_file",
        type=str,
        required=True,
        help="Path to input file",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=False,
        default=None,
        help="Path to log file",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.DEBUG)

    my_file = args.input_file
    logging.info(f"DfAnalyzer server at: {os.environ.get('DFA_URL')} <> input file is: {my_file}")

    dataflow_tag = "soma-df"
    df = Dataflow(dataflow_tag)
    # Proveniência prospectiva
    # Transformação para extração dos números do arquivo: ExtrairNumeros
    tf1 = Transformation("ExtrairNumeros")
    tf1_input = Set("iExtrairNumeros", SetType.INPUT, [Attribute("SOMA_FILE", AttributeType.FILE)])
    tf1_output = Set("oExtrairNumeros", SetType.OUTPUT, [Attribute("PRIMEIRO_NUMERO", AttributeType.NUMERIC),
                                                         Attribute("SEGUNDO_NUMERO", AttributeType.NUMERIC)])
    tf1.set_sets([tf1_input, tf1_output])
    df.add_transformation(tf1)

    # Transformação para somar os dois números extraídos do arquivo: ExecutarSoma
    tf2 = Transformation("ExecutarSoma")
    tf1_output.set_type(SetType.INPUT)
    tf1_output.dependency = tf1._tag
    tf2_output = Set("oExecutarSoma", SetType.OUTPUT, [Attribute("RESULTADO_SOMA", AttributeType.NUMERIC)])
    tf2.set_sets([tf1_output, tf2_output])
    df.add_transformation(tf2)

    df.save()

    # Proveniência retrospectiva
    start = time.process_time()
    t1 = Task(1, dataflow_tag, "ExtrairNumeros")
    t1_input = DataSet("iExtrairNumeros", [Element([my_file])])
    t1.add_dataset(t1_input)
    t1.begin()
    with open(my_file) as f:
        lista = f.read().split()
        PRIMEIRO_NUMERO = int(lista[0])
        SEGUNDO_NUMERO = int(lista[1])

    t1_output = DataSet("oExtrairNumeros", [Element([PRIMEIRO_NUMERO, SEGUNDO_NUMERO])])
    t1.add_dataset(t1_output)
    t1.end()

    t2 = Task(2, dataflow_tag, "ExecutarSoma", dependency=t1)
    t2.begin()
    RESULTADO_SOMA = PRIMEIRO_NUMERO + SEGUNDO_NUMERO
    t2_output = DataSet("oExecutarSoma", [Element([RESULTADO_SOMA])])
    t2.add_dataset(t2_output)
    t2.end()

    logging.info(f"DfAnalyzer time (sec) = {time.process_time() - start}")
    # gros: 9.452 ms, rpi3: 132.87 ms, a8: 350.66 ms
    # 189x, 149x, 147x


import argparse
import logging
import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--number_of_tasks",
        type=int,
        required=True,
        help="Number of tasks.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=True,
        help="Number of data values per task (each data value is about the same size).",
    )
    parser.add_argument(
        "--task_duration",
        type=float,
        required=True,
        help="Duration of the task in secs.",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=True,
        help="Path to log file.",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.DEBUG)

    start = time.time()

    for _ in range(args.number_of_tasks):
        time.sleep(args.task_duration)
        # time_begin = time.time()
        # while time.time() - time_begin < args.task_duration:
        #     continue

    end = time.time()

    logging.info(f"Synthetic workload baseline: [task_duration={args.task_duration} / "
                 f"number_of_tasks={args.number_of_tasks} / "
                 f"data_values_per_task={args.data_values_per_task}] "
                 f"execution time (sec) = {end - start}")

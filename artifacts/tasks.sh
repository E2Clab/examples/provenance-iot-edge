#!/bin/bash
for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --log /tmp/provlake-out-05s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/provlake-out-1s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 3.5 --log /tmp/provlake-out-35s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLAKE_URL=http://[$1]:22000 && python /tmp/synthetic-workload-provlake.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 5 --log /tmp/provlake-out-5s.log
   sleep $2
done

for i in {1..10}
do
   export DFA_URL=http://[$1]:22000 && python /tmp/synthetic-workload-dfanalyzer.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --log /tmp/dfa-out-05s.log
   sleep $2
done

for i in {1..10}
do
   export DFA_URL=http://[$1]:22000 && python /tmp/synthetic-workload-dfanalyzer.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/dfa-out-1s.log
   sleep $2
done

for i in {1..10}
do
   export DFA_URL=http://[$1]:22000 && python /tmp/synthetic-workload-dfanalyzer.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 3.5 --log /tmp/dfa-out-35s.log
   sleep $2
done

for i in {1..10}
do
   export DFA_URL=http://[$1]:22000 && python /tmp/synthetic-workload-dfanalyzer.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 5 --log /tmp/dfa-out-5s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --log /tmp/provlight-out-05s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/provlight-out-1s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 3.5 --log /tmp/provlight-out-35s.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 5 --log /tmp/provlight-out-5s.log
   sleep $2
done

# TODO: Python Client for MQTT-SN brokers
#   FIXME: AF_INET6 and IPPROTO_IPV6 in "/root/mqttsn/src/mqttsn/client.py"
# https://github.com/luanguimaraesla/mqttsn.git
# git clone https://github.com/luanguimaraesla/mqttsn.git
# pip3 install -U -e mqttsn/
import zlib
import logging
from mqttsn.client import Client, Callback
import argparse

logging.basicConfig(level=logging.INFO)


class DfaTaskResource(Callback):
    def message_arrived(self, topic_name, payload, qos, retained, msgid):
        print(f'{self} | topic_name: {topic_name} | msgid {msgid} | '
              f'qos {qos} | retained {retained} | payload: {payload}')
        # self.dfa_save(payload.decode('ascii'))
        self.dfa_save(zlib.decompress(payload).decode('ascii'))
        return True

    def dfa_save(self, message):
        """ Send a post request to the Dataflow Analyzer API to store the Task.
        """
        import requests
        r = requests.post("http://localhost:22000/pde/task/json", json=message)
        print(r.status_code)


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--client_id",
        type=str,
        required=True,
        help="Client ID.",
    )
    args = parser.parse_args()

    aclient = Client(f"cloud_sub{args.client_id}", port=1883)
    aclient.register_callback(DfaTaskResource())
    aclient.connect()
    rc, topic1 = aclient.subscribe(topic=f"provlight{args.client_id}", qos=0)
    print("*" * 80 + f"topic id is: {topic1}")

    try:
        while True:
            continue
    except KeyboardInterrupt:
        print("Closing the client")
        aclient.disconnect()

import socket
from http.server import HTTPServer, BaseHTTPRequestHandler


class SimpleHTTPRequestHandler(BaseHTTPRequestHandler):

    def do_POST(self):
        if self.path in [
            "/pde/dataflow/json",  # DfAnalyzer: dataflow
            "/pde/task/json",  # DfAnalyzer: task
            "/retrospective-provenance?with_validation=False",  # ProvLake dataflow/task
            # f"{SERVER_API_ROOT}",  # ProvLake: dataflow
            # f"{SERVER_API_ROOT}",  # ProvLake: task
        ]:
            print(self.headers)
            print('POST payload: %s' %
                  self.rfile.read(int(self.headers['Content-Length'])).decode("UTF-8"))
            self.send_response(200)
            # self.send_header('Content-type', 'text/html')
            # self.send_header("Connection", "keep-alive")
            # self.send_header("keep-alive", "3")
            self.end_headers()
            self.wfile.write(b'okk')


# SERVER_API_ROOT = "/provenance/api"
# # PROSPECTIVE ROUTES
# METAMODEL_LOAD = "/metamodel/load"
# PROJECTS = f"{SERVER_API_ROOT}/projects"
# DATA_STORES = "/data-stores"
# QUERY_MANAGEMENT = f"{SERVER_API_ROOT}/stored-queries"
# WORKFLOWS = "/workflows"
# DATA_TRANSFORMATIONS = "/data-transformations"
# # RETROSPECTIVE ROUTES
# CYCLE_EXECUTIONS = f"{SERVER_API_ROOT}" \
#                    f"/workflow-executions/<path:workflow_execution_id>/cycle-executions"
# DATA_TRANSFORMATION_EXECUTIONS = "/data-transformation-executions"
# DATA_TRANSFORMATION_EXECUTIONS_BY_PERSON = \
#     "/persons/<path:person_id>/data-transformation-executions"
# WORKFLOW_EXECUTIONS = "/workflow-executions"
# GLOBAL_WORKFLOW_EXECUTION = "/global-workflow-execution"
# WORKFLOW_EXECUTIONS_BY_WORKFLOW_EXECUTION = f"{WORKFLOW_EXECUTIONS}" \
#                                             f"/<path:workflow_execution_id>"
# WORKFLOW_EXECUTIONS_SUMMARY_BY_WORKFLOW_EXECUTION = \
#     "/workflow-execution-summaries/<path:workflow_execution_id>"

# httpd = HTTPServer(('localhost', 22000), SimpleHTTPRequestHandler)
# print(f"Server running at: http://{httpd.server_address}:{httpd.server_port}")
# httpd.serve_forever()

class HTTPServerV6(HTTPServer):
    address_family = socket.AF_INET6


httpd = HTTPServerV6(('::', 22000), SimpleHTTPRequestHandler)
print(f"Server running at: http://{httpd.server_address}:{httpd.server_port}")
httpd.serve_forever()

#!/bin/bash
for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 10 --log /tmp/provlight-out-05s-b10.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 10 --log /tmp/provlight-out-1s-b10.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 20 --log /tmp/provlight-out-05s-b20.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 20 --log /tmp/provlight-out-1s-b20.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 0.5 --bag 50 --log /tmp/provlight-out-05s-b50.log
   sleep $2
done

for i in {1..10}
do
   export PROVLIGHT_SERVER_URL=$1 && python /tmp/synthetic-workload-provlight-bag.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --bag 50 --log /tmp/provlight-out-1s-b50.log
   sleep $2
done

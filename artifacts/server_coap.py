import logging
import asyncio
import aiocoap.resource as resource
import aiocoap

logging.basicConfig(level=logging.INFO)
logging.getLogger("coap-server").setLevel(logging.DEBUG)


class DfaTaskResource(resource.Resource):
    def __init__(self):
        super().__init__()

    async def render_post(self, request):
        message = request.payload
        print('POST payload: %s' % message)
        self.dfa_save(request.payload.decode('ascii'))
        return aiocoap.Message(code=aiocoap.CHANGED, payload=b'1')

    def dfa_save(self, message):
        """ Send a post request to the Dataflow Analyzer API to store the Task.
        """
        import requests
        r = requests.post("http://localhost:22000/pde/task/json", json=message)
        print(r.status_code)


async def main():
    root = resource.Site()
    root.add_resource(['pde', 'task', 'json'], DfaTaskResource())
    await aiocoap.Context.create_server_context(root)
    await asyncio.get_running_loop().create_future()

if __name__ == "__main__":
    asyncio.run(main())

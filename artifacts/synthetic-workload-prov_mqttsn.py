from mqttsn.client import Client, Callback
import argparse
import logging
import time
from dfa_lib_python.dataflow import Dataflow
from dfa_lib_python.transformation import Transformation
from dfa_lib_python.attribute import Attribute
from dfa_lib_python.attribute_type import AttributeType
from dfa_lib_python.set import Set
from dfa_lib_python.set_type import SetType
from dfa_lib_python.task import Task
from dfa_lib_python.dataset import DataSet
from dfa_lib_python.element import Element


class MyCallback(Callback):
    def message_arrived(self, topic_name, payload, qos, retained, msgid):
        return True


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--number_of_transformations",
        type=int,
        required=True,
        help="Number of transformations.",
    )
    parser.add_argument(
        "--number_of_tasks",
        type=int,
        required=True,
        help="Number of tasks.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=True,
        help="Number of data values per task (each data value is about the same size).",
    )
    parser.add_argument(
        "--task_duration",
        type=float,
        required=True,
        help="Duration of the task in secs.",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=True,
        help="Path to log file.",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.INFO)

    dataflow_tag = "synthetic-workload-prov"
    df = Dataflow(dataflow_tag)
    # NOTE: Prospective Provenance
    for _id in range(args.number_of_transformations):
        transformation = Transformation(f"tf{_id}")
        transformations_input = Set(f"itf{_id}" if _id < 1 else f"otf{_id-1}", SetType.INPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                        for attribute in range(args.data_values_per_task)])
        if _id > 0:
            transformations_input.dependency = f"tf{_id-1}".lower()
        transformations_output = Set(f"otf{_id}", SetType.OUTPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                     for attribute in range(args.data_values_per_task)])
        transformation.set_sets([transformations_input, transformations_output])
        df.add_transformation(transformation)
    # df.save()
    # Open MQTT-SN connection
    import os
    dfa_url_mqttsn = os.environ.get('DFA_URL_MQTTSN', "2001:660:4406:500:9::f")
    context = Client("edge", host=dfa_url_mqttsn, port=1883)
    context.register_callback(MyCallback())
    context.connect()  # clean_session=False
    rc, topic_id = context.subscribe("prov")
    # NOTE: Retrospective Provenance
    input_task_elements = [1 for _ in range(args.data_values_per_task)]
    output_task_elements = [2 for _ in range(args.data_values_per_task)]
    start = time.time()
    previous_task = None

    _task_container = []
    for _id_tf in range(args.number_of_transformations):
        for _id_tk in range(int(args.number_of_tasks/args.number_of_transformations)):
            task = Task(_id_tk, dataflow_tag, f"tf{_id_tf}", dependency=previous_task)
            task.add_dataset(DataSet(f"itf{_id_tf}", [Element(input_task_elements)]))
            task.begin_light_mqttsn(context, topic_id)
            time.sleep(args.task_duration)
            task.add_dataset(DataSet(f"otf{_id_tf}", [Element(output_task_elements)]))
            task.end_light()
            previous_task = task
            _task_container.append(task)
            if len(_task_container) == 1:
                task.end_light_mqttsn_container(context, topic_id, _task_container)
                _task_container = []

    end = time.time()
    context.disconnect()

    logging.info(f"Synthetic workload capture [PROVLIGHT]: [task_duration={args.task_duration} / "
                 f"number_of_transformations={args.number_of_transformations} / "
                 f"number_of_tasks={args.number_of_tasks} / "
                 f"data_values_per_task={args.data_values_per_task}] "
                 f"execution time (sec) = {end - start}")

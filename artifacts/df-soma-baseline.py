import argparse
import logging
import time

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="Dfa")
    parser.add_argument(
        "--input_file",
        type=str,
        required=True,
        help="Path to input file",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=False,
        default=None,
        help="Path to log file",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.DEBUG)

    my_file = args.input_file

    start = time.process_time()

    with open(my_file) as f:
        lista = f.read().split()
        PRIMEIRO_NUMERO = int(lista[0])
        SEGUNDO_NUMERO = int(lista[1])

    RESULTADO_SOMA = PRIMEIRO_NUMERO + SEGUNDO_NUMERO
    # for i in range(10000):
    #     for j in range(10000):
    #         pass
    logging.info(f"Baseline time (sec) = {time.process_time() - start}")
    # gros: 0.05 ms, rpi3: 0.89 ms, a8: 2.38 ms

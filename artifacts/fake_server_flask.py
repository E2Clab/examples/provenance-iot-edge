from flask import Flask, make_response
from werkzeug.serving import WSGIRequestHandler
from flask import request

app = Flask(__name__)


def read_respond():
    print(request.data)
    print("\n")
    return make_response("{'1':'a'}")


@app.route("/")
def liveness():
    return "Server up!"


@app.route('/pde/dataflow/json', methods=['POST'])
def dfanalyzer_dataflow():
    return read_respond()


@app.route('/pde/task/json', methods=['POST'])
def dfanalyzer_task():
    return read_respond()


@app.route('/retrospective-provenance', methods=['POST'])
def provlake():
    return read_respond()


if __name__ == '__main__':
    WSGIRequestHandler.protocol_version = "HTTP/1.1"
    app.run(host='::', port=22000, debug=True)

import argparse
import logging
import time
from dfa_lib_python.dataflow import Dataflow
from dfa_lib_python.transformation import Transformation
from dfa_lib_python.attribute import Attribute
from dfa_lib_python.attribute_type import AttributeType
from dfa_lib_python.set import Set
from dfa_lib_python.set_type import SetType
from dfa_lib_python.task import Task
from dfa_lib_python.dataset import DataSet
from dfa_lib_python.element import Element

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--task_duration",
        type=float,
        required=True,
        help="Duration of the task in secs.",
    )
    parser.add_argument(
        "--number_of_tasks",
        type=int,
        required=True,
        help="Number of tasks.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=True,
        help="Number of data values per task (each data value is about the same size).",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=True,
        help="Path to log file.",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.DEBUG)

    dataflow_tag = "synthetic-workload-prov"
    df = Dataflow(dataflow_tag)
    # NOTE: Prospective Provenance
    for _id in range(args.number_of_tasks):
        transformation = Transformation(f"tf{_id}")
        transformations_input = Set(f"itf{_id}" if _id < 1 else f"otf{_id-1}", SetType.INPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                        for attribute in range(args.data_values_per_task)])
        if _id > 0:
            transformations_input.dependency = f"tf{_id-1}".lower()
        transformations_output = Set(f"otf{_id}", SetType.OUTPUT,
                                    [Attribute(f"attribute{attribute}", AttributeType.NUMERIC)
                                     for attribute in range(args.data_values_per_task)])
        transformation.set_sets([transformations_input, transformations_output])
        df.add_transformation(transformation)
    df.save()
    # NOTE: Retrospective Provenance
    input_task_elements = [1 for _ in range(args.data_values_per_task)]
    output_task_elements = [2 for _ in range(args.data_values_per_task)]
    start = time.time()
    previous_task = None
    for _id in range(args.number_of_tasks):
        task = Task(_id, dataflow_tag, f"tf{_id}", dependency=previous_task)
        task.add_dataset(DataSet(f"itf{_id}", [Element(input_task_elements)]))
        task.begin()
        time.sleep(args.task_duration)
        task.add_dataset(DataSet(f"otf{_id}", [Element(output_task_elements)]))
        task.end()
        previous_task = task

    logging.info(f"Synthetic workload capture: [task_duration={args.task_duration} / "
                 f"number_of_tasks={args.number_of_tasks} / "
                 f"data_values_per_task={args.data_values_per_task}] "
                 f"execution time (sec) = {time.time() - start}")

import argparse
import logging
import time
from provlight.workflow import Workflow
from provlight.task import Task
from provlight.data import Data


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description="synthetic-workload")
    parser.add_argument(
        "--number_of_transformations",
        type=int,
        required=True,
        help="Number of transformations.",
    )
    parser.add_argument(
        "--number_of_tasks",
        type=int,
        required=True,
        help="Number of tasks.",
    )
    parser.add_argument(
        "--data_values_per_task",
        type=int,
        required=True,
        help="Number of data values per task (each data value is about the same size).",
    )
    parser.add_argument(
        "--task_duration",
        type=float,
        required=True,
        help="Duration of the task in secs.",
    )
    parser.add_argument(
        "--bag",
        type=int,
        required=True,
        help="Bag size.",
    )
    parser.add_argument(
        "--log",
        type=str,
        required=True,
        help="Path to log file.",
    )
    args = parser.parse_args()

    if args.log is not None:
        logging.basicConfig(filename=args.log, level=logging.INFO)

    # NOTE: Prospective Provenance
    wf = Workflow("synthetic", grouping=args.bag)
    wf.begin()
    # NOTE: Retrospective Provenance
    input_task_elements = {'in': [1 for _ in range(args.data_values_per_task)]}
    output_task_elements = {'out': [2 for _ in range(args.data_values_per_task)]}
    start = time.time()
    data_id = 0
    previous_task = []

    for _id_tf in range(args.number_of_transformations):
        for _id_tk in range(int(args.number_of_tasks/args.number_of_transformations)):
            data_id += 1
            data_in = Data(f"in_{data_id}", wf.id, [], input_task_elements)
            task = Task(f"tf{_id_tf}_tk{_id_tk}", wf, f"tf{_id_tf}", dependencies=previous_task)
            task.begin([data_in])
            time.sleep(args.task_duration)
            data_out = Data(f"out_{data_id}", wf.id, [], output_task_elements)
            task.end([data_out])
            previous_task = [task.id]

    end = time.time()
    wf.end()

    logging.info(f"Synthetic workload capture [PROVLIGHT]: [task_duration={args.task_duration} / "
                 f"number_of_transformations={args.number_of_transformations} / "
                 f"number_of_tasks={args.number_of_tasks} / "
                 f"data_values_per_task={args.data_values_per_task}] "
                 f"execution time (sec) = {end - start}")

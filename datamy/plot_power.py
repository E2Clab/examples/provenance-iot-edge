# scp drosendo@grenoble.iot-lab.info:/senslab/users/drosendo/.iot-lab/344033/consumption/a8_103.oml /home/drosendo/git/provenance-iot-edge/datamy/a8_103-1.oml
# plot_oml_consum -p -i datamy/a8_103.oml -b 0 -e 2500

# python /tmp/synthetic-workload-baseline.py --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/client-out-v2.log
# 100.10776591300964

# export DFA_URL=http://[2001:660:4406:500:9::12]:22000/ && python /tmp/synthetic-workload-prov.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/client-out-v2.log
# 122.6892638206482

# export DFA_URL_COAP=coap://[2001:660:4406:500:9::12]:5683 && export DFA_URL=http://[2001:660:4406:500:9::12]:22000/ && python /tmp/synthetic-workload-prov_light.py --number_of_transformations 5 --number_of_tasks 100 --data_values_per_task 100 --task_duration 1 --log /tmp/client-out-v2.log
# 110.69023299217224

# INFO:root:Synthetic workload baseline: [task_duration=1.0 / number_of_tasks=600 / data_values_per_task=100] execution time (sec) = 600.6585977077484
# INFO:root:Synthetic workload capture: [task_duration=1.0 / number_of_tasks=600 / data_values_per_task=100] execution time (sec) = 729.4143257141113
# INFO:root:Synthetic workload capture: [task_duration=1.0 / number_of_transformations=5 / number_of_tasks=600 / data_values_per_task=100] execution time (sec) = 661.8590316772461

import time

time_begin = time.time()
while time.time() - time_begin < 1:
    continue

end = time.time()
print(f"execution time (sec) = {end - time_begin}")

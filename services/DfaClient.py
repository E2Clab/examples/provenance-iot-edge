from e2clab.services import Service
import enoslib as en


class DfaClient(Service):

    def deploy(self):
        with en.actions(roles=[]) as a:
        #with en.actions(roles=self.roles) as a:
            # install DfAnalyzer python lib
            # a.pip(name="git+https://gitlab.com/ssvitor/dataflow_analyzer/-/tree/master/library/dfa-lib-python")
            a.git(repo="https://gitlab.com/ssvitor/dataflow_analyzer.git", dest="~/dataflow_analyzer/")
            a.shell("cp -rf dataflow_analyzer/library/dfa-lib-python . && rm -rf dataflow_analyzer/")
            a.shell("pip3 install -e dfa-lib-python/")
            # install ProvLake python lib
            a.git(repo="https://github.com/IBM/multi-data-lineage-capture-py.git", dest="~/multi-data-lineage-capture-py/")
            a.shell("echo 'requests-futures' > multi-data-lineage-capture-py/requirements.txt")
            a.shell("pip3 install -e multi-data-lineage-capture-py/")
            # install MQTT-SN client
            # a.pip(name="git+https://github.com/luanguimaraesla/mqttsn")
            a.git(repo="https://github.com/luanguimaraesla/mqttsn.git", dest="~/mqttsn/")
            a.shell("pip3 install -U -e mqttsn/")
            # install ProvLight python lib
            # a.pip(name="git+https://drosendo:glpat-1Je6n5C1-z3Vk_HYzsv2@gitlab.inria.fr/provlight/provlight")
            a.git(repo="https://drosendo:glpat-1Je6n5C1-z3Vk_HYzsv2@gitlab.inria.fr/provlight/provlight.git", dest="~/provlight/")
            a.shell("pip3 install -e provlight/")

        return self.register_service()


# from e2clab.services import Service
# import enoslib as en
#
#
# class DfaClient(Service):
#
#     def deploy(self):
#         with en.actions(roles=self.roles) as a:
#             # install DfAnalyzer python lib
#             a.shell("git clone https://gitlab.com/ssvitor/dataflow_analyzer.git")
#             a.shell("cp -rf dataflow_analyzer/library/dfa-lib-python . && rm -rf dataflow_analyzer/")
#             a.shell("pip3 install -e dfa-lib-python/")
#             # install ProvLake python lib
#             a.shell("git clone https://github.com/IBM/multi-data-lineage-capture-py.git")
#             a.shell("echo 'requests-futures' > multi-data-lineage-capture-py/requirements.txt")
#             a.shell("pip3 install -e multi-data-lineage-capture-py/")
#             # install MQTT-SN client
#             a.shell("git clone https://github.com/luanguimaraesla/mqttsn.git")
#             a.shell("pip3 install -U -e mqttsn/")
#             # install ProvLight python lib
#             a.shell("git clone https://gitlab.inria.fr/provlight/provlight.git")
#             # a.shell("git clone https://drosendo:glpat-1Je6n5C1-z3Vk_HYzsv2@gitlab.inria.fr/provlight/provlight.git")
#             a.shell("pip3 install -e provlight/")
#             # install CoAP python lib
#             # a.shell("git clone https://github.com/chrysn/aiocoap")
#             # a.shell("mv aiocoap aiocoap-local")
#             # a.shell("pip3 install -e aiocoap-local/")
#
#         return self.register_service()

# git clone https://github.com/renan-souza/simple-uwsgi-flask.git
# cd simple-uwsgi-flask
# add all routes "app.rout(...)" to main.py
# TODO: SERVER
# OPTION 1
# FIXME update uwsgi.ini: http = 0.0.0.0:22000 processes = 32 threads = 32
# docker build -t simple_uwsgi_flask .
# docker run --memory="80g" --cpus="32" -v `pwd`:/app -p 22000:22000 -it simple_uwsgi_flask
# OPTION 2
# /home/drosendo/git/e2clab-examples/dfanalyzer/artifacts/fake_server_flask.py main.py
# OPTION 3
# pip install uwsgi
# pip install flask
# uwsgi --http [::]:22000 --wsgi-file /tmp/fake_server_flask_v2.py --callable app --master --processes 32 --enable-threads --stats 127.0.0.1:9191
# telnet 127.0.0.1 9191
# curl -6 'http://[2001:660:4406:500:9::13]:22000/'
# pkill -f uwsgi -9
# TODO PROVLIGHT
# --- python3 /tmp/fake_server.py &
# cd /root/mosquitto.rsmb/rsmb/src/ && ./broker_mqtts config.conf &
# python3 /tmp/server_mqttsn.py &

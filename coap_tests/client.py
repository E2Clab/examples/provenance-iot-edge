import logging
import asyncio

from aiocoap import *

logging.basicConfig(level=logging.INFO)

async def main():
    protocol = await Context.create_client_context()

    request = Message(code=GET, uri='coap://localhost/time')

    try:
        response = await protocol.request(request).response
    except Exception as e:
        print('Failed to fetch resource:')
        print(e)
    else:
        print('Result: %s\n%r'%(response.code, response.payload))

if __name__ == "__main__":
    asyncio.run(main())

# import logging
# import asyncio
# import os
# from aiocoap import *
#
# logging.basicConfig(level=logging.INFO)
# URL_DFA_SERVER = os.environ.get('DFA_URL', "coap://0.0.0.0")
# URL_RESOURCE_TASK = '/pde/task/json'
#
#
# async def main():
#     protocol = await Context.create_client_context()
#     url = URL_DFA_SERVER + URL_RESOURCE_TASK
#     request = Message(code=GET, uri=url)
#
#     try:
#         response = await protocol.request(request).response
#     except Exception as e:
#         print('Failed to fetch resource:')
#         print(e)
#     else:
#         print('Result: %s\n%r'%(response.code, response.payload))
#
# if __name__ == "__main__":
#     asyncio.run(main())

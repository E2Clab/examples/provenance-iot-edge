import logging
import asyncio
from aiocoap import *
import os
import time

logging.basicConfig(level=logging.INFO)
# URL_DFA_SERVER = os.environ.get('DFA_URL', "coap://[2001:660:4406:500:3::38]:5683")
URL_DFA_SERVER = "coap://localhost:5683"
URL_RESOURCE_TASK = '/pde/task/json'

# https://stackoverflow.com/questions/55277431/python-convert-dictionary-to-bytes
# _json = {}
# _json['aa'] = "aaa"
# print(type(_json))
# print(_json)
# message = str(_json)
# ascii_message = message.encode('ascii')
# print(type(ascii_message))
# print(ascii_message)
# print("---------------")
# new_m = ascii_message.decode('ascii')
# print(type(new_m))
# print(new_m)
# import json
# json_str = json.dumps(new_m)
# print(type(json_str))
# print(f"json_str={json_str}")
# import base64
# output_byte = base64.b64encode(ascii_message)
# print(type(output_byte))
# print(output_byte)
# exit()


async def send_message(context, _message, _url):
    request = Message(code=POST, payload=_message, uri=_url)
    response = await context.request(request).response
    print('Response: %s\n%r' % (response.code, response.payload))


async def main():
    context = await Context.create_client_context()
    url = URL_DFA_SERVER + URL_RESOURCE_TASK
    start = time.time()
    for _ in range(1):
        await send_message(context, b"start", url)
        time.sleep(1)  # await asyncio.sleep(1)
        await send_message(context, b"end", url)
    end = time.time()
    print(f"execution time (sec) = {end - start}")


if __name__ == "__main__":
    asyncio.run(main())


# TODO: RSMB: Really Small Message Broker https://github.com/eclipse/mosquitto.rsmb
# git clone https://github.com/eclipse/mosquitto.rsmb.git
# cd /root/mosquitto.rsmb/rsmb/src/ && make
# ./broker_mqtts config.conf

# TODO: Edit a file config.conf (vim config.conf) with the following content:
#
# # add some debug output
# trace_output protocol
#
# # listen for MQTT-SN traffic on UDP port 1885
# listener 1885 INADDR_ANY mqtts
#   ipv6 true
#
# # listen to MQTT connections on tcp port 1886
# listener 1886 INADDR_ANY
#   ipv6 true

# TODO: option 2
# # Uncomment this to show you packets being sent and received
# #trace_output protocol
#
# # Normal MQTT listener
# listener 1883 INADDR_ANY
# ipv6 true
#
# # MQTT-SN listener
# listener 1883 INADDR_ANY mqtts
# ipv6 true


import argparse
import logging
from mqttsn.client import Client, Callback
import sys
# TODO: Python Client for MQTT-SN brokers
#   FIXME: AF_INET6 and IPPROTO_IPV6 in "/home/root/mqttsn/src/mqttsn/client.py"
# https://github.com/luanguimaraesla/mqttsn.git
# git clone https://github.com/luanguimaraesla/mqttsn.git
# pip3 install -U -e mqttsn/
# python3 /tmp/client_mqttsn.py --host 2001:660:4406:500:9::f
# TODO: server side: config.conf
# TODO: MQTT-SN Tools: https://github.com/njh/mqtt-sn-tools
# git clone https://github.com/njh/mqtt-sn-tools.git
# cd mqtt-sn-tools/ && make
# ./mqtt-sn-pub -h 2001:660:4406:500:9::f -p 1883 -t test/riot -m iotlab
# TODO: tutorials
# https://www.iot-lab.info/legacy/tutorials/riot-mqtt-sn-a8-m3/index.html
# https://github.com/marcocerino/IotAssignment1/tree/master/MQTTSNbridge


logging.basicConfig(level=logging.DEBUG)


class MyCallback(Callback):
    def message_arrived(self, topic_name, payload, qos, retained, msgid):
        print(f'{self} | topic_name: {topic_name} | payload: {payload} | '
              f'qos {qos} | retained {retained} | msgid {msgid}',
              file=sys.stderr)

        return True


if __name__ == '__main__':
    parser = argparse.ArgumentParser(description="MQTT-SN")
    parser.add_argument(
        "--host",
        type=str,
        required=True,
        help="MQTT server address.",
    )
    args = parser.parse_args()

    # create MQTT-SN client and connect to RSMB
    aclient = Client("a8", host=args.host, port=1883)
    aclient.register_callback(MyCallback())
    aclient.connect()

    rc, topic1 = aclient.subscribe("topic1")
    print("topic id for topic1 is", topic1)
    aclient.publish(topic1, "aaaa", qos=0)
    aclient.unsubscribe("topic1")
    aclient.publish(topic1, "dddd", qos=0)

    # infinite loop fo rthe MQTT-SN client
    try:
        while True:
            # time.sleep(2)
            continue
    except KeyboardInterrupt:
        print("Closing the client")
        aclient.disconnect()


# payload: "{
# 'tag': 'tf4',
# 'dependency': {
#       'tags': [{'tag': 'tf4'}],
#       'ids': [{'id': '18'}]
#  },
#  'sets': [
#       {'tag': 'itf4',
#        'elements': [
#               ['1', '1']
#         ]
#       }
#  ],
#  'status': 'RUNNING',
#  'dataflow': 'synthetic-workload-prov',
#  'transformation': 'tf4',
#  'id': '19'
#  }
# ------------------------------------------------------------------------------------------
# payload: "[{
# 'tag': 'tf4',
# 'dependency': {  FIXME: duplicated
#       'tags': [{'tag': 'tf4'}],
#       'ids': [{'id': '18'}]
#  },
#  'sets': [
#       {'tag': 'otf4',
#        'elements': [
#               ['2', '2']
#         ]
#       }
#  ],
#  'status': 'FINISHED',  FIXME: S, F
#  'dataflow': 'synthetic-workload-prov',  FIXME: duplicated
#  'transformation': 'tf4',  FIXME: duplicated
#  'id': '19',  FIXME: duplicated
#  'performances': [  FIXME: raw time
#       {'startTime': '2022-11-17 22:18:59', 'endTime': '2022-11-17 22:19:03'}
#   ]
#  }
# ]
# ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------
# payload: [
# {"prov_obj":
#    {
#           "task": {
#               "id": 1668720316.9618685,
#               "wf_execution": null,
#               "generatedTime": 1668720316.9618685
#           },
#           "dt": "tf4_tk19",
#           "type": "Generate",
#           "values": {
#               "in_task": [1, 1]
#           }
#    },
#    "dataflow_name": null,
#    "act_type": "task"
# }
# ]
# payload: [
# {"prov_obj":
#    {
#           "task": {
#               "id": 1668720316.9618685,
#               "wf_execution": null,
#               "startTime": 1668720316.964046,
#               "endTime": 1668720317.9671268,
#               "generatedTime": 1668720316.9618685,
#               "status": "FINISHED"
#            },
#            "dt": "tf4_tk19",
#            "type": "Output",
#            "values": {
#                "out_task": [2, 2]
#             }
#    },
#    "dataflow_name": null,
#    "act_type": "task"
# }
# ]

import zlib
import time
import sys

a = "{'tag': 'tf4', 'dependency': {'tags': [{'tag': 'tf4'}], 'ids': [{'id': '18'}]}, 'sets': [{'tag': 'otf4', 'elements': [['2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2']]}], 'status': 'FINISHED', 'dataflow': 'synthetic-workload-prov', 'transformation': 'tf4', 'id': '19', 'performances': [{'startTime': '2022-11-19 16:09:24', 'endTime': '2022-11-19 16:09:25'}]}"
print(sys.getsizeof(a))
start = time.time()
a = zlib.compress(a.encode('ascii'))
end = time.time()
print(f"compression time (sec) = {end - start}")
print(type(a))
print(sys.getsizeof(a))
print(zlib.decompress(a).decode('ascii'))
exit()

import time
start = time.time()
t = time.localtime()
# print(t)
print(f"execution time (sec) = {time.time() - start}")

from datetime import datetime
start = time.time()
# dt = datetime.now()
# dt = str(datetime.now())
dt = datetime.now().strftime('%Y-%m-%d %H:%M:%S')
# print(datetime.now())print(f"execution time (sec) = {time.time() - start}")
# if len(dt) > 0:
#     print(f"execution time (sec) = {time.time() - start}")
print(f"execution time (sec) = {time.time() - start}")


# import os
# start = time.time()
# d = os.system('date')
# # print(d)
# print(f"execution time (sec) = {time.time() - start}")
exit()





# _message = []
# _message.append({'1': 'a'})
# _message.append({'1': 'b'})
_message = str(_message).encode('ascii')
print(_message)

exit()

_sets = []
print(len(_sets))
print(len(_sets))
exit()

# message = self.get_specification()
# message = {'tag': 'tf4', 'dependency': {'tags': [{'tag': 'tf4'}], 'ids': [{'id': '18'}]}, 'sets': [{'tag': 'itf4', 'elements': [['1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1']]}, {'tag': 'otf4', 'elements': [['2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2']]}], 'status': 'FINISHED', 'dataflow': 'synthetic-workload-prov', 'transformation': 'tf4', 'id': '19', 'performances': [{'startTime': '2022-11-04 11:22:36', 'endTime': '2022-11-04 11:22:37'}]}
message = {'tag': 'tf4',
           'dependency': {'tags': [{'tag': 'tf4'}], 'ids': [{'id': '18'}]},
           'sets': [{'tag': 'itf1', 'elements': [['1']]},
                    {'tag': 'itf2', 'elements': [['2']]},
                    {'tag': 'itf3', 'elements': [['3']]},
                    ],
           'status': 'FINISHED',
           'dataflow': 'synthetic-workload-prov',
           'transformation': 'tf4', 'id': '19',
           'performances': [{'startTime': '2022-11-04 11:22:36',
                             'endTime': '2022-11-04 11:22:37'}]
           }
task_end = True
print(type(message))
print(len(message['sets']))
_input_add_dataset = len(message['sets'])
print(type(message['sets']))
print(f"*** [IN] sets sent = {message['sets']}")
print('-' * 80)

message = {'tag': 'tf4',
           'dependency': {'tags': [{'tag': 'tf4'}], 'ids': [{'id': '18'}]},
           'sets': [{'tag': 'itf1', 'elements': [['1']]},
                    {'tag': 'itf2', 'elements': [['2']]},
                    {'tag': 'itf3', 'elements': [['3']]},
                    {'tag': 'otf1', 'elements': [['4']]},
                    {'tag': 'otf2', 'elements': [['5']]},
                    ],
           'status': 'FINISHED',
           'dataflow': 'synthetic-workload-prov',
           'transformation': 'tf4', 'id': '19',
           'performances': [{'startTime': '2022-11-04 11:22:36',
                             'endTime': '2022-11-04 11:22:37'}]
           }

# message['sets'].pop(0) if len(message['sets']) == 2 else None
if task_end:
    del message['sets'][:_input_add_dataset]

print(len(message['sets']))
print(f"*** [OUT] sets sent = {message['sets']}")
print('-' * 80)
message = str(message).encode('ascii')
print(type(message))
print(message)

# import time
# start_send_message = time.time()
# n = 5
# mylist = [1,2,3,4,5,6,7,8,9]
# newlist = mylist[n:]
# end_send_message = time.time()
# print(f"send_message = {end_send_message - start_send_message}")
# print(newlist)
#
# start_send_message = time.time()
# n = 5
# mylist = [1,2,3,4,5,6,7,8,9]
# del mylist[:n]
# end_send_message = time.time()
# print(f"send_message = {end_send_message - start_send_message}")
# print(mylist)


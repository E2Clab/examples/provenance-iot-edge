import logging
import asyncio
from aiocoap import *
import os

logging.basicConfig(level=logging.INFO)
URL_DFA_SERVER = os.environ.get('DFA_URL', "coap://localhost")
URL_RESOURCE_TASK = '/pde/task/json'


async def main():
    context = await Context.create_client_context()
    await asyncio.sleep(2)
    url = URL_DFA_SERVER + URL_RESOURCE_TASK
    # payload = b"The quick brown fox jumps over the lazy dog.\n" * 1
    payload = b"{'tag': 'tf99', 'dependency': {'tags': [{'tag': 'tf98'}], 'ids': [{'id': '0'}]}, 'sets': [{'tag': 'itf99', 'elements': [['1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1', '1']]}, {'tag': 'otf99', 'elements': [['2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2', '2']]}], 'status': 'FINISHED', 'dataflow': 'synthetic-workload-prov', 'transformation': 'tf99', 'id': '0', 'performances': [{'startTime': '2022-11-01 16:14:15', 'endTime': '2022-11-01 16:14:16'}]}"
    request = Message(code=POST, payload=payload, uri=url)
    response = await context.request(request).response
    print('Response: %s\n%r' % (response.code, response.payload))


if __name__ == "__main__":
    asyncio.run(main())


import logging
import asyncio
import aiocoap.resource as resource
import aiocoap

logging.basicConfig(level=logging.INFO)
logging.getLogger("coap-server").setLevel(logging.DEBUG)


class DfaTaskResource(resource.Resource):
    def __init__(self):
        super().__init__()

    async def render_post(self, request):
        print('POST payload: %s' % request.payload)
        # TODO: add provenance data to DfA DB.
        return aiocoap.Message(code=aiocoap.CHANGED, payload=b'task saved!')


async def main():
    root = resource.Site()
    root.add_resource(['pde', 'task', 'json'], DfaTaskResource())
    await aiocoap.Context.create_server_context(root)
    await asyncio.get_running_loop().create_future()

if __name__ == "__main__":
    asyncio.run(main())
